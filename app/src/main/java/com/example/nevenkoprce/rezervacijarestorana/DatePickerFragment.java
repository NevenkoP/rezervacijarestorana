package com.example.nevenkoprce.rezervacijarestorana;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;
import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Stvori novi objekt tipa DatePickerDialog i vrati ga
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    // kada se izabere datum
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // ažuriraj polje izabraniDatum kod pozivatelja s izabranim datumom
        ((TextView) getActivity().findViewById(R.id.izaberiDatum)).setText(day+"."+(month+1)+"."+year);
    }
}

