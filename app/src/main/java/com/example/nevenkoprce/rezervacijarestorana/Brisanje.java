package com.example.nevenkoprce.rezervacijarestorana;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class Brisanje extends AppCompatActivity {

    private EditText pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brisanje);
        pin = (EditText)findViewById(R.id.pinUnos);

    }

    public void brisi(View v){
        SharedPreferences sp = this.getSharedPreferences("rezervacija", MODE_PRIVATE);
        String pin_brisi = pin.getText().toString();
        if(!sp.contains(pin_brisi)){
            Toast.makeText(Brisanje.this, "Rezervacija s pinom "+pin.getText().toString()+" ne postoji!", Toast.LENGTH_SHORT).show();
        }else{
            SharedPreferences.Editor ed = sp.edit();
            ed.remove(pin_brisi);
            ed.apply();
            Toast.makeText(Brisanje.this, "Rezervacija sa pinom:"+ pin.getText().toString()+ " je izbrisana!", Toast.LENGTH_SHORT).show();
        }
    }

    public void vratiSeNaPocetnu(View v){
        finish();
    }

}
