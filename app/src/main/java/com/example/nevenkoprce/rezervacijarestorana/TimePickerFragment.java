package com.example.nevenkoprce.rezervacijarestorana;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.Calendar;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        // Stvori novi objekt tipa TimePickerDialog i vrati ga
        return new TimePickerDialog(getActivity(), this, hour, min, DateFormat.is24HourFormat(getActivity()));
    }

    // kada se izabere vrijeme
    @Override
    public void onTimeSet(TimePicker view, int hour, int minute) {
        // ažuriraj polje izabrani na aktivnosti pozivatelja s izabranim vremena
        ((TextView) getActivity().findViewById(R.id.izaberiVrijeme)).setText(hour+":"+minute);
    }
}

