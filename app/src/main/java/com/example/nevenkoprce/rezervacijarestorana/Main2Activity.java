package com.example.nevenkoprce.rezervacijarestorana;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Random;

public class Main2Activity extends AppCompatActivity {
    private EditText pin;
    private EditText restoran;
    private TextView datum;
    private TextView vrijeme;
    private EditText brojOsoba;
    private EditText ime;
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        pin = (EditText)findViewById(R.id.pinUnos);
        restoran = (EditText)findViewById(R.id.restoranUnos);
        datum = (TextView)findViewById(R.id.izaberiDatum);
        vrijeme = (TextView)findViewById(R.id.izaberiVrijeme);
        brojOsoba = (EditText)findViewById(R.id.unosBrojOsoba);
        ime = (EditText)findViewById(R.id.unosIme);
        //SharedPreferences sp = this.getSharedPreferences("rezervacija", MODE_PRIVATE);


    }

    public void birajDatum(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void birajVrijeme(View v){
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(),"timePicker");
    }

    public int randPin(){
        int min = 1000;
        int max = 9999;
        int randomPin = new Random().nextInt((max - min) + 1) + min;
        return randomPin;
    }

    public void spremi(View v) {
        if (!provjeriEkranskaPolja()) {
            Toast.makeText(this, "Morate popuniti sva ekranska polja!", Toast.LENGTH_LONG).show();
            return;
        }
        SharedPreferences sp = this.getSharedPreferences("rezervacija", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        Rezervacija rez = new Rezervacija();
        rez.setPin(Integer.parseInt(pin.getText().toString()));
        rez.setRestoran(restoran.getText().toString());
        rez.setDatum(datum.getText().toString());
        rez.setVrijeme(vrijeme.getText().toString());
        rez.setBrojOsoba(Integer.parseInt(brojOsoba.getText().toString()));
        rez.setIme(ime.getText().toString());
        gson = new Gson();
        String json = gson.toJson(rez);
        String kljuc = rez.getPin()+"";
        ed.putString(kljuc,json);
        ed.apply();
        Toast.makeText(Main2Activity.this, "Rezervacija sa pinom:"+ pin.getText().toString()+ " je zaprimljena!", Toast.LENGTH_SHORT).show();

    }

    private boolean provjeriEkranskaPolja(){
        if (pin.getText().toString() == null || pin.getText().toString().length() == 0)
            return false;
        if (restoran.getText().toString() == null || restoran.getText().toString().length() == 0)
            return false;
        if (datum.getText().toString() == null || datum.getText().toString().length() == 0)
            return false;
        if (vrijeme.getText().toString() == null || vrijeme.getText().toString().length() == 0)
            return false;
        if (brojOsoba.getText().toString() == null || brojOsoba.getText().toString().length() == 0)
            return false;
        if (ime.getText().toString() == null || ime.getText().toString().length() == 0)
            return false;
        return true;
    }

    public void vratiSeNaPocetnu(View v){
        finish();
    }




}
