package com.example.nevenkoprce.rezervacijarestorana;

import java.util.Random;

public class Rezervacija {
    private int pin;
    private String restoran;
    private String datum;
    private String vrijeme;
    private int brojOsoba;
    private String ime;

    public Rezervacija(){

    }

    public int getPin(){
        return pin;
    }

    public void setPin(int pin) {
        this.pin=pin;
    }

    public String getRestoran() {
        return restoran;
    }

    public void setRestoran(String restoran) {
        this.restoran=restoran;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum=datum;
    }

    public String getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(String vrijeme) {
        this.vrijeme=vrijeme;
    }

    public int getBrojOsoba() {
        return brojOsoba;
    }

    public void setBrojOsoba(int brojOsoba) {
        this.brojOsoba=brojOsoba;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime=ime;
    }

    /*public void randPin(int minimum){
        int min = 1000;
        int max = 9999;
        int randomPin = new Random().nextInt((max - min) + 1) + min;
    }*/



}
