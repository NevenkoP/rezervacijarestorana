package com.example.nevenkoprce.rezervacijarestorana;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class Citanje extends AppCompatActivity {
    private EditText pin;
    private EditText restoran;
    private TextView datum;
    private TextView vrijeme;
    private EditText brojOsoba;
    private EditText ime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citanje);
        pin = (EditText)findViewById(R.id.pinUnos);
        restoran = (EditText)findViewById(R.id.restoranUnos);
        datum = (EditText) findViewById(R.id.izaberiDatum);
        vrijeme = (EditText) findViewById(R.id.izaberiVrijeme);
        brojOsoba = (EditText)findViewById(R.id.unosBrojOsoba);
        ime = (EditText)findViewById(R.id.unosIme);
    }

    public void citaj(View v){
        SharedPreferences sp = this.getSharedPreferences("rezervacija", MODE_PRIVATE);
        String pin_citaj = pin.getText().toString();
        if (!sp.contains(pin_citaj)){
            Toast.makeText(Citanje.this, "Rezervacija s pinom: "+pin_citaj+" ne postoji u spremištu!", Toast.LENGTH_SHORT).show();
        }
        else{
            Gson gson = new Gson();
            String json = sp.getString(pin_citaj+"","");
            Rezervacija rez = gson.fromJson(json,Rezervacija.class);
            pin.setText(String.valueOf(rez.getPin()));
            restoran.setText(rez.getRestoran());
            datum.setText(rez.getDatum());
            vrijeme.setText(rez.getVrijeme());
            brojOsoba.setText(String.valueOf(rez.getBrojOsoba()));
            ime.setText(rez.getIme());
            Toast.makeText(Citanje.this, "Učitavanje uspješno!", Toast.LENGTH_SHORT).show();
        }
    }

    public void vratiSeNaPocetnu(View v){
        finish();
    }


}
