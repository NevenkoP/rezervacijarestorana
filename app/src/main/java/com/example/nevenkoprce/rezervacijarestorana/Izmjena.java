package com.example.nevenkoprce.rezervacijarestorana;

import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class Izmjena extends AppCompatActivity {
    private EditText pin;
    private EditText restoran;
    private TextView datum;
    private TextView vrijeme;
    private EditText brojOsoba;
    private EditText ime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izmjena);
        pin = (EditText)findViewById(R.id.pinUnos);
        restoran = (EditText)findViewById(R.id.restoranUnos);
        datum = (EditText) findViewById(R.id.izaberiDatum);
        vrijeme = (EditText) findViewById(R.id.izaberiVrijeme);
        brojOsoba = (EditText)findViewById(R.id.unosBrojOsoba);
        ime = (EditText)findViewById(R.id.unosIme);
    }

    public void birajDatum(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void birajVrijeme(View v){
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(),"timePicker");
    }
    public void izmjena(View v){
        String pin_iz = pin.getText().toString();
        Gson gson = new Gson();
        SharedPreferences sp = this.getSharedPreferences("rezervacija", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        Rezervacija rez = gson.fromJson(sp.getString(pin_iz+"",""),Rezervacija.class);
        if (!sp.contains(pin_iz)){
            Toast.makeText(Izmjena.this, "Rezervacija s pinom: "+pin.getText().toString()+"ne postoji u spremištu!", Toast.LENGTH_SHORT).show();
            return;
        }else{
            rez.setPin(Integer.parseInt(pin.getText().toString()));
            rez.setRestoran(restoran.getText().toString());
            rez.setDatum(datum.getText().toString());
            rez.setVrijeme(vrijeme.getText().toString());
            rez.setBrojOsoba(Integer.parseInt(brojOsoba.getText().toString()));
            rez.setIme(ime.getText().toString());
            String json = gson.toJson(rez);
            String kljuc = rez.getPin()+"";
            ed.putString(kljuc,json);
            ed.apply();
            Toast.makeText(Izmjena.this, "Rezervacija sa pinom:"+ pin.getText().toString()+ " je zaprimljena!", Toast.LENGTH_SHORT).show();
        }


        ed.apply();
        Toast.makeText(Izmjena.this, "Rezervacija sa pinom:"+ pin.getText().toString()+ " je izmjenjena!", Toast.LENGTH_SHORT).show();

    }
    public void vratiSeNaPocetnu(View v){
        finish();
    }
}
