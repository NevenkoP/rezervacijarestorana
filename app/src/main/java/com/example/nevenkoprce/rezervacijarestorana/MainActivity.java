package com.example.nevenkoprce.rezervacijarestorana;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pozoviAtivnostUnos(View v){
        Intent i = new Intent(this, Main2Activity.class);
        startActivity(i);
    }

    public void pozoviAktivnostIzmjena(View v){
        Intent i = new Intent(this, Izmjena.class);
        startActivity(i);
    }

    public void pozivAktivnistBrisanje(View v){
        Intent i = new Intent(this,Brisanje.class);
        startActivity(i);
    }

    public void pozivAktivnostiCitanje(View v){
        Intent i = new Intent(this,Citanje.class);
        startActivity(i);
    }
}
